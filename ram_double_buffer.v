`timescale 1ns / 1ps

module ram_double_buffer #(
    parameter data_width=8,
    parameter address_width=2
    )
    (
     input                     buffer,
    /* a interface */
     input                     A_clk,
     input                     A_enable,
     input                     A_write_enable,
     input [address_width-1:0] A_address,
     input [data_width-1:0]    A_data_in,
    output [data_width-1:0]    A_data_out,
    /* b interface */
     input                     B_clk,
     input                     B_enable,
     input                     B_write_enable,
     input [address_width-1:0] B_address,
     input [data_width-1:0]    B_data_in,
    output [data_width-1:0]    B_data_out
    );

    /* rams */
    wire f_clk, f_enable, f_write_enable;
    wire [address_width-1:0] f_address;
    wire [data_width-1:0] f_data_in, f_data_out;
    ram_single_port #(.data_width(data_width), .address_width(address_width)) front
                     (.clk(f_clk), .enable(f_enable), .write_enable(f_write_enable),
                      .address(f_address), .data_in(f_data_in), .data_out(f_data_out));

    wire b_clk, b_enable, b_write_enable;
    wire [address_width-1:0] b_address;
    wire [data_width-1:0] b_data_in, b_data_out;
    ram_single_port #(.data_width(data_width), .address_width(address_width)) back
                     (.clk(b_clk), .enable(b_enable), .write_enable(b_write_enable),
                      .address(b_address), .data_in(b_data_in), .data_out(b_data_out));

    /* mux */
    /* front */
    assign f_clk=(!buffer)?A_clk:B_clk;
    assign f_enable=(!buffer)?A_enable:B_enable;
    assign f_write_enable=(!buffer)?A_write_enable:B_write_enable;
    assign f_address=(!buffer)?A_address:B_address;
    assign f_data_in=(!buffer)?A_data_in:B_data_in;
    /* back */
    assign b_clk=(!buffer)?B_clk:A_clk;
    assign b_enable=(!buffer)?B_enable:A_enable;
    assign b_write_enable=(!buffer)?B_write_enable:A_write_enable;
    assign b_address=(!buffer)?B_address:A_address;
    assign b_data_in=(!buffer)?B_data_in:A_data_in;
    /* data outputs */
    assign A_data_out=(!buffer)?f_data_out:b_data_out;
    assign B_data_out=(!buffer)?b_data_out:f_data_out;

endmodule
