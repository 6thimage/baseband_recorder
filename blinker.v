`timescale 1ns / 1ps

module blinker(
     input clk,
     input reset,
     input [1:0] rate,
    output reg blink
    );

    wire clk_div_hit;
    mod_m_counter #(.m(5_000_000), .reset_pulse(0)) clk_div
                   (.clk(clk), .reset(reset), .enable(1'b1), .hit(clk_div_hit));

    reg [6:0] counter='d0;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            counter <= 'd0;
            blink <= 1'b0;
        end
        else if(clk_div_hit)
        begin
            /* make counter go from 0 to 99 */
            if(counter=='d99)
                counter <= 'd0;
            else
                counter <= counter + 1'b1;

            /* fastest rate */
            if(rate=='d0)
                blink <= ~blink;
            else if(rate=='d1 && (counter%2)==0)
                blink <= ~blink;
            else if(rate=='d2 && (counter%10)==0)
                blink <= ~blink;
            /* slowest rate */
            else if(rate=='d3)
            begin
                if(counter=='d0)
                    blink <= 1'b1;
                else if(counter=='d2)
                    blink <= 1'b0;
            end
        end
    end

endmodule
