`timescale 1ns / 1ps

module cmd_sender(
     input           clk,
     input           reset,
    /* control and data input */
     input           enable,
     input     [7:0] data,          /* not internally stored, must be held constant while busy is high */
    /* connection to clock divider */
     input           clock_strobe,
    /* connections to sd card */
     input           sclk,
    output reg       cmd_line,
    /* busy flag */
    output           busy
    );

    /* initial command line output */
    initial cmd_line=1'b1;

    /* counter to store number of bits sent */
    reg [2:0] bit_counter=3'd7;

    /* states */
    localparam STATE_IDLE=0, STATE_ENABLED=1;
    reg state=STATE_IDLE;

    assign busy=(state==STATE_ENABLED)?1'b1:1'b0;

    /* state machine */
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* reset output */
            cmd_line <= 1'b1;

            /* reset counter */
            bit_counter <= 3'd7;

            /* reset internal state */
            state <= STATE_IDLE;
        end
        else
        begin
            case(state)
            STATE_IDLE:
            begin
                if(enable)
                begin
                    /* reset counter */
                    bit_counter <= 3'd7;

                    /* move to next state */
                    if(sclk)
                        state <= STATE_ENABLED;
                end
            end

            STATE_ENABLED:
            begin
                if(clock_strobe)
                begin
                    /* check for end condition */
                    if(!sclk)
                    begin
                        if(bit_counter==3'd0)
                        begin
                            state <= STATE_IDLE;
                        end
                        else
                            /* increment counter */
                            bit_counter <= bit_counter - 1'b1;
                    end
                    /* output data on falling edge */
                    else if(sclk)
                        /* output data */
                        cmd_line <= data[bit_counter];
                end
            end
            endcase
        end
    end

endmodule
