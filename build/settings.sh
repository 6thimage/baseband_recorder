#! /bin/bash

output_name="baseband_recorder"
part="xc6slx45-csg324-3"
constraints="../pipistrello_v2.01.ucf"
ngdbuild_search_dir="../ip"
fpga_bscan="/home/ian/Documents/postgrad/dev boards/pipistrello/fpgaprog/bscan_spi_lx45_csg324.bit"

map_options="-logic_opt off -ol high -t 1 -xt 0 -register_duplication off -r 4 -global_opt off -ir off -pr off -lc off -power off"
par_options="-ol high"
trce_options="-v 3 -s 3 -n 3 -fastpaths"

bitgen_options="-g UserID:0xFFFFFFFF -g ConfigRate:26 -g SPI_buswidth:4 -g TIMER_CFG:0xFFFF -g DONE_cycle:4 -g DonePipe:Yes -g Security:None"

xilinx_settings="/opt/Xilinx/14.7/ISE_DS/settings64.sh"

# prefix ngdbuild search dir with -sd
if [ ! -z "$ngdbuild_search_dir" ]; then
    # trim leading and tailing whitespace
    ngdbuild_search_dir=${ngdbuild_search_dir%% }
    ngdbuild_search_dir=${ngdbuild_search_dir## }
    # add prefix
    ngdbuild_search_dir="-sd ${ngdbuild_search_dir// / -sd }"
fi
