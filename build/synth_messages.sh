#! /bin/bash

synth_log=baseband_recorder.syr
line_count='wc -l'
proj_dir="/home/ian/programming/fpga/baseband_recorder/"

echo '*****************************************************************************************'
# limit to errors
errors=$(grep -i '^[ ]*error' $synth_log | sed 's/^[ ]*//g')
# remove directory
errors=$(echo "$errors" | sed "s|$proj_dir||g")

# output to screen
echo " Errors: $(if [ -z "$errors" ]; then echo -en '0'; else echo "$errors"|$line_count; fi)"
echo '*****************************************************************************************'
echo "$errors"

echo '*****************************************************************************************'
# limit to warnings
warnings=$(grep -i '^[ ]*warning' $synth_log | sed 's/^[ ]*//g')
# apply filtering
# remove directory
warnings=$(echo "$warnings" | sed "s|$proj_dir||g")
# remove warnings about ip/lpddr
warnings=$(echo "$warnings" | grep -v -e 'ip/lpddr' -e 'c3_p[2-5].*' -e 's[0-5]_axi.*' \
           -e 'p[2-5]_.*' -e 'ui_.*' -e 'mig_.*' -e 'mcbx_dram_.*' \
           -e 'mcb_raw_wrapper' -e 'mcb_soft_calibration' -e 'iodrp_.*controller' \
           -e 'sys_clk_[pn]' -e 'calib_recal' -e 'ZIO_.*' -e 'MCB_.*' -e 'drp_ioi_addr' \
           -e 'syn_preserve' \
           )

# output to screen
echo " Warnings: $(if [ -z "$warnings" ]; then echo -en '0'; else echo "$warnings"|$line_count; fi)"
echo '*****************************************************************************************'
echo "$warnings"

echo '*****************************************************************************************'
# limit to infos
infos=$(grep -i '^[ ]*info' $synth_log | sed 's/^[ ]*//g')
# apply filtering
# remove directory
infos=$(echo "$infos" | sed "s|$proj_dir||g")
# remove infos about ip/lpddr
infos=$(echo "$infos" | grep -v -e 'ip/lpddr' -e 'mcb_soft_calibration' \
        -e 'iodrp_.*controller' -e 'unit infrastructure' \
        )

# output to screen
echo " Infos: $(if [ -z "$infos" ]; then echo -en '0'; else echo "$infos"|$line_count; fi)"
echo '*****************************************************************************************'
echo "$infos"

