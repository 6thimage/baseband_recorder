`timescale 1ns / 1ps

module mod_m_counter(
     input clk,
     input reset,
     input enable,
    output hit
    );

    /* m as a parameter */
    parameter m = 10;
    /* option to make the first pulse occur after the reset */
    parameter reset_pulse = 0;
    /* size of counter required */
    localparam n = log2(m);

    /* register for counter */
    reg [n-1:0] counter=0;
    /* hit is high when we need to reset the counter */
    assign hit=(counter==m-1);

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
            counter<=reset_pulse?m-1:0;
        else
        begin
            if(enable)
            begin
                if(hit)
                    counter<=0;
                else
                    counter<=counter+1'b1;
            end
        end
    end

    /* this function is only used at synthesis to calculate
       the number of bits required for the counter */
    function integer log2(input integer n);
        integer i;
        begin
            log2=1;
            for(i=0; 2**i<n; i=i+1)
                log2=i+1;
        end
    endfunction

endmodule
