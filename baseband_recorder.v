`timescale 1ns / 1ps
module baseband_recorder(
     input clkin,
    /* LPDDR */
     inout mcb3_dram_dqs,
     inout mcb3_dram_udqs,
     inout mcb3_rzq,
     inout [15:0] mcb3_dram_dq,
    output [12:0] mcb3_dram_a,
    output [1:0] mcb3_dram_ba,
    output mcb3_dram_ck,
    output mcb3_dram_ck_n,
    output mcb3_dram_cke,
    output mcb3_dram_ras_n,
    output mcb3_dram_cas_n,
    output mcb3_dram_we_n,
    output mcb3_dram_dm,
    output mcb3_dram_udm,
    /* sd card */
    output sd_clk,
     inout sd_cmd,
     inout [3:0] sd_dat,
    /* max2769 */
     //input Wing_C0, Wing_C1, Wing_C2, Wing_C3, Wing_C4,
     //input Wing_A0, Wing_A1, Wing_A2, Wing_A3, Wing_C0,
     input Wing_A7, Wing_A10, Wing_A11, Wing_A13, Wing_A14,
    /* spi */
     //output Wing_C13, Wing_C14, Wing_C15,
     output Wing_A0, Wing_A1, Wing_A2,
    /* shutdown & idle */
     output Wing_A3, Wing_A4,
    /* push button */
     input switch,
    /* LEDs */
    output LED1, LED2, LED3, LED4,
    output reg LED5
    );

    /* max2769 aliases */
    //wire data_clk=Wing_C0;
    //wire [1:0] I={Wing_C3, Wing_C4}; /* {I+, I-} {I1, I0} */
    //wire [1:0] Q={Wing_C1, Wing_C2}; /* {Q+, Q-} {Q1, Q0} */
    /*wire [1:0] I={Wing_A2, Wing_A3};
    wire [1:0] Q={Wing_A0, Wing_A1};*/
    wire data_clk=Wing_A7;
    wire [1:0] I={Wing_A14, Wing_A13};
    wire [1:0] Q={Wing_A10, Wing_A11};
    /* TODO: need to add grounded pins */

    /* spi aliases */
    wire ss, sclk, mosi;
    assign Wing_A0=mosi;
    assign Wing_A1=sclk;
    assign Wing_A2=ss;
    /*assign Wing_C15=ss;
    assign Wing_C14=sclk;
    assign Wing_C13=mosi;*/

    /* shutdown & idle */
    wire shutdown, idle;
    assign Wing_A3=shutdown;
    assign Wing_A4=idle;

    /* these are complement/inverted signals */
    assign shutdown=1'b1;
    assign idle=1'b1;

    /* 512 Mb (64 MB) LPDDR ram
     * 
     * The read and write address for the command ports have the bottom 3 bits
     * set to zero, this is due to the use of 64 bit ports. In addition to
     * this, the top 4 bits are also set to zero, which is due to the ram size
     * being 512 Mb.
     * 
     * This code sets the bottom 6 bits to zero, as reading and writing is
     * done in blocks of 8, resulting in the lower six bits always being zero.
     * So rather than let the synthesis tool detect them as constant, we do it
     * in the code.
     *
     * The LPDDR ram module also controls the system clock and the reset, the
     * value of the clock is set by a divider in lpddr.v and the reset is
     * combined with the calibration done signal to form the reset for the
     * main logic (this is the line under the module instantiation).
     */
    wire clk, reset_in, reset_out, calib_done,
         lpddr_wr_en,
         /* control flags */
         lpddr_wr_cmd_empty, lpddr_rd_cmd_empty, lpddr_rd_empty,
         /* wr error flags */
         lpddr_wr_cmd_full, lpddr_wr_full, lpddr_wr_underrun, lpddr_wr_error,
         /* rd error flags */
         lpddr_rd_cmd_full, lpddr_rd_full, lpddr_rd_overflow, lpddr_rd_error;
    reg lpddr_rd_en=1'b0, lpddr_wr_cmd_en=1'b0, lpddr_rd_cmd_en=1'b0;
    reg [20:0] lpddr_wr_address=21'd0, lpddr_rd_address=21'd0;
    wire [63:0] lpddr_wr_data, lpddr_rd_data;
    lpddr #(.C3_P0_MASK_SIZE(8), .C3_P0_DATA_PORT_SIZE(64),
            .C3_P1_MASK_SIZE(8), .C3_P1_DATA_PORT_SIZE(64),
            .DEBUG_EN(0), .C3_MEMCLK_PERIOD(5000), .C3_CALIB_SOFT_IP("TRUE"),
            .C3_SIMULATION("FALSE"), .C3_RST_ACT_LOW(0), .C3_INPUT_CLK_TYPE("SINGLE_ENDED"),
            .C3_MEM_ADDR_ORDER("ROW_BANK_COLUMN"), .C3_NUM_DQ_PINS(16), .C3_MEM_ADDR_WIDTH(13),
            .C3_MEM_BANKADDR_WIDTH(2))
          lpddr_ram(
            .c3_sys_clk(clkin), .c3_sys_rst_i(reset_in),
            .mcb3_dram_dq(mcb3_dram_dq), .mcb3_dram_a(mcb3_dram_a), .mcb3_dram_ba(mcb3_dram_ba),
            .mcb3_dram_ras_n(mcb3_dram_ras_n), .mcb3_dram_cas_n(mcb3_dram_cas_n),
            .mcb3_dram_we_n(mcb3_dram_we_n), .mcb3_dram_cke(mcb3_dram_cke),
            .mcb3_dram_ck(mcb3_dram_ck), .mcb3_dram_ck_n(mcb3_dram_ck_n),
            .mcb3_dram_dqs(mcb3_dram_dqs), .mcb3_dram_udqs(mcb3_dram_udqs),
            .mcb3_dram_udm(mcb3_dram_udm), .mcb3_dram_dm(mcb3_dram_dm),
            .c3_clk0(clk), .c3_rst0(reset_out),
            .c3_calib_done(calib_done),
            .mcb3_rzq(mcb3_rzq),
            /* port 0 */
            /* cmd */
            .c3_p0_cmd_clk(clk), .c3_p0_cmd_en(lpddr_wr_cmd_en),
            .c3_p0_cmd_instr(3'd0), .c3_p0_cmd_bl(6'd7),
            .c3_p0_cmd_byte_addr({4'd0,lpddr_wr_address[19:0],6'd0}),
            .c3_p0_cmd_empty(lpddr_wr_cmd_empty), .c3_p0_cmd_full(lpddr_wr_cmd_full),
            /* wr */
            .c3_p0_wr_clk(clk), .c3_p0_wr_en(lpddr_wr_en),
            .c3_p0_wr_mask(8'd0), .c3_p0_wr_data(lpddr_wr_data),
            .c3_p0_wr_full(lpddr_wr_full), .c3_p0_wr_empty(),
            .c3_p0_wr_count(), .c3_p0_wr_underrun(lpddr_wr_underrun),
            .c3_p0_wr_error(lpddr_wr_error),
            /* rd */
            .c3_p0_rd_clk(1'b0), .c3_p0_rd_en(1'b0),
            .c3_p0_rd_data(), .c3_p0_rd_full(),
            .c3_p0_rd_empty(), .c3_p0_rd_count(),
            .c3_p0_rd_overflow(), .c3_p0_rd_error(),
            /* port 1 */
            /* cmd */
            .c3_p1_cmd_clk(clk), .c3_p1_cmd_en(lpddr_rd_cmd_en),
            .c3_p1_cmd_instr(3'd1), .c3_p1_cmd_bl(6'd7),
            .c3_p1_cmd_byte_addr({4'd0,lpddr_rd_address[19:0],6'd0}),
            .c3_p1_cmd_empty(lpddr_rd_cmd_empty), .c3_p1_cmd_full(lpddr_rd_cmd_full),
            /* wr */
            .c3_p1_wr_clk(1'b0), .c3_p1_wr_en(1'b0),
            .c3_p1_wr_mask(8'd0), .c3_p1_wr_data(64'd0),
            .c3_p1_wr_full(), .c3_p1_wr_empty(),
            .c3_p1_wr_count(), .c3_p1_wr_underrun(),
            .c3_p1_wr_error(),
            /* rd */
            .c3_p1_rd_clk(clk), .c3_p1_rd_en(lpddr_rd_en),
            .c3_p1_rd_data(lpddr_rd_data), .c3_p1_rd_full(lpddr_rd_full),
            .c3_p1_rd_empty(lpddr_rd_empty), .c3_p1_rd_count(),
            .c3_p1_rd_overflow(lpddr_rd_overflow), .c3_p1_rd_error(lpddr_rd_error));
    assign reset=!(reset_out|~calib_done);

    /* these error signals are generic, catch-anything, and should, in normal
     * operation, always stay low
     */
    wire wr_error=(lpddr_wr_cmd_full|lpddr_wr_full|lpddr_wr_underrun|lpddr_wr_error);
    wire rd_error=(lpddr_rd_cmd_full|lpddr_rd_full|lpddr_rd_overflow|lpddr_rd_error);

    ///* signal gen */
    ////wire clk_32_759;
    ////dcm clk_dcm(.clkin(clk), .clk_32_759(clk_32_759));
    //reg data_clk=1'b0;
    //reg [1:0] I='d0, Q='d0;
    ////reg [3:0] prescale='d0;
    //always @(posedge clk)//_32_759)
    //begin
    //    data_clk <= ~data_clk;
    //    if(!data_clk)
    //    begin
    //        //I <= I + 1'b1;
    //        //Q <= Q - 1'b1;
    //        if(prescale=='d0)
    //            {I, Q} <= {I, Q} + 1'b1;

    //        prescale <= prescale + 1'b1;
    //    end
    //end

    /* baseband acquisition
     * 
     * The baseband module synchronises the input I, Q and data clock to the
     * system clock and then writes it into a 64 bit register. When the
     * register has been filled, the write enable signal is pulsed high for
     * one clock cycle. This pulse is used to fill the LPDDR fifo.
     */
    reg baseband_enable=1'b0;
    baseband base(.clk(clk), .reset(reset), .enable(baseband_enable),
                  .data_clk(data_clk), .I(I), .Q(Q),
                  .wr_en(lpddr_wr_en), .wr_data(lpddr_wr_data));

    always @(posedge clk) if(lpddr_wr_en) LED5 <= (lpddr_wr_data==64'd0);

    /* ram write logic
     * 
     * This logic keeps track of the number of pulses of the baseband's write
     * enable and, on the 8th pulse, pulses the write enable of the LPDDR's
     * command port. This loads the preset command to write the 8 64-bit words
     * into the ram at the current address. On completion of this (when the
     * command fifo becomes empty), the write address is incremented.
     */
    reg [2:0] wr_en_counter=3'd0;
    reg inc_wr_address=1'b0;
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            wr_en_counter <= 3'd0;
            lpddr_wr_cmd_en <= 1'b0;
            lpddr_wr_address <= 21'd0;
            inc_wr_address <= 1'b0;
        end
        else
        begin
            if(lpddr_wr_en)
            begin
                /* increment counter */
                wr_en_counter <= wr_en_counter + 1'b1;
    
                if(wr_en_counter == 3'd7)
                begin
                    /* start a ram write */
                    lpddr_wr_cmd_en <= 1'b1;
    
                    /* increment ram address */
                    inc_wr_address <= 1'b1;
                end
            end

            if(lpddr_wr_cmd_en)
                lpddr_wr_cmd_en <= 1'b0;
    
            if(inc_wr_address && lpddr_wr_cmd_empty)
            begin
                inc_wr_address <= 1'b0;
                lpddr_wr_address <= lpddr_wr_address + 1'b1;
            end
        end
    end

    /* sd card controller */
    wire sd_ram_clk, sd_ram_enable, sd_ram_write_enable;
    wire [8:0] sd_ram_address;
    wire [7:0] sd_ram_data_in, sd_ram_data_out;
    reg data_command_enable=1'b0, write_next=1'b0;
    wire sd_card_detect, last_block, sd_idle, write_next_idle;
    wire [3:0] sd_error;
    wire [31:0] card_block_size;
    sd_controller sd(.clk(clk), .reset(reset),
                     .sclk(sd_clk), .cmd(sd_cmd), .data(sd_dat), .card_detect(sd_card_detect),
                     .ram_clk(sd_ram_clk), .ram_enable(sd_ram_enable),
                         .ram_write_enable(sd_ram_write_enable), .ram_address(sd_ram_address),
                         .ram_data_in(sd_ram_data_in), .ram_data_out(sd_ram_data_out),
                     .data_command_enable(data_command_enable), .write_block(1'b1),
                         .erase_blocks(1'b0), .block_address(32'd0), .multiple_blocks(1'b1),
                         .fixed_count(1'b0), .block_count(32'd0), .last_block(last_block),
                         .data_next(write_next),
                     .error(sd_error), .idle(sd_idle), .data_next_idle(write_next_idle),
                     .card_version_2(), .card_hc(), .card_uhs(), .card_block_size(card_block_size));

    /* SPI master */
    reg spi_enable=1'b0, spi_cont;
    reg [7:0] spi_data;
    wire spi_busy;
    spi_master spi(.clk(clk),
                   .ss(ss), .sclk(sclk), .mosi(mosi), .miso(1'b0),
                   .reset(reset), .enable(spi_enable), .clock_div(8'd500), .cpol(1'b0), .cpha(1'b1),
                   .tx_data(spi_data), .rx_data(), .data_cont(spi_cont), .busy(spi_busy));

    reg [31:0] spi_rom[9:0];
    initial
    begin
        /* lna 1 */
        //spi_rom[0]={28'ha2952a3, 4'b0000};
        //spi_rom[0]={28'hbff52a3, 4'b0000}; /* 4.290 MHz, max current */

        //spi_rom[3]={28'h9ec0008, 4'b0011};
        //spi_rom[3]={28'h9ec0208, 4'b0011}; /* set ICP to 1 ma rather than 0.5 ma */

        /* lna 1 xtal/2 */
        //spi_rom[0]={28'ha295563, 4'b0000};
        //spi_rom[3]={28'h9cc0008, 4'b0011};

        /* lna 2 */
        //spi_rom[0]={28'ha293aa3, 4'b0000};
        spi_rom[0]={28'hbff3aa3, 4'b0000}; /* 4.290 MHz, max current*/

        spi_rom[3]={28'h9ec0008, 4'b0011};
        //spi_rom[3]={28'h9ec0208, 4'b0011}; /* set ICP to 1 ma rather than 0.5 ma */

        /* lna 2 xtal/2 */
        //spi_rom[0]={28'ha293d63, 4'b0000};
        //spi_rom[0]={28'hbff3d63, 4'b0000}; /* max current */
        //spi_rom[3]={28'h9cc0008, 4'b0011};

        /* general */ 
        //spi_rom[1]={28'h8550288, 4'b0001}; /* 2 bits IQ */
        //spi_rom[1]={28'h8550308, 4'b0001}; /* 3 bits I */
        spi_rom[1]={28'h8290308, 4'b0001}; /* 3 bits I gain ref 82*/ 
        //spi_rom[2]={28'heaff1dc, 4'b0010};
        spi_rom[2]={28'heaff000, 4'b0010}; /* remove stream settings */
        spi_rom[4]={28'h0c00080, 4'b0100};
        spi_rom[5]={28'h8000070, 4'b0101};
        spi_rom[6]={28'h8000000, 4'b0110};
        spi_rom[7]={28'h10061b2, 4'b0111};
        /* these last two are test registers, which shouldn't need to be sent
         * (as they are the default)
         */
        spi_rom[8]={28'h1e0f401, 4'b1000};
        spi_rom[9]={28'h14c0402, 4'b1001};
    end

    /* SPI control */
    wire spi_init, spi_complete;
    localparam SPI_IDLE=0, SPI_SEND=1, SPI_WAIT=2, SPI_COMPLETE=3;
    reg [1:0] spi_state=SPI_IDLE;
    reg [3:0] spi_rom_counter=4'd0;
    reg [1:0] spi_byte_counter=2'd0;
    assign spi_complete=(spi_state==SPI_COMPLETE);
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* reset spi enable */
            spi_enable <= 1'b0;

            /* reset counters */
            spi_rom_counter <= 4'd0;
            spi_byte_counter <= 2'd0;
        end
        else
        begin
            case(spi_state)
                SPI_IDLE:
                begin
                    if(spi_init)
                    begin
                        spi_state <= SPI_SEND;
                        spi_rom_counter <= 4'd0;
                        spi_byte_counter <= 4'd0;
                    end
                end

                SPI_SEND:
                begin
                    case(spi_byte_counter)
                        2'd0: spi_data <= spi_rom[spi_rom_counter][31:24];
                        2'd1: spi_data <= spi_rom[spi_rom_counter][23:16];
                        2'd2: spi_data <= spi_rom[spi_rom_counter][15:8];
                        2'd3: spi_data <= spi_rom[spi_rom_counter][7:0];
                    endcase

                    spi_enable <= 1'b1;

                    if(spi_byte_counter==2'd3)
                        spi_cont <= 1'b0;
                    else
                        spi_cont <= 1'b1;

                    spi_byte_counter <= spi_byte_counter + 1'b1;

                    if(spi_byte_counter==2'd3)
                        spi_rom_counter <= spi_rom_counter + 1'b1;

                    spi_state <= SPI_WAIT;
                end

                SPI_WAIT:
                begin
                    if(spi_enable && spi_busy)
                        spi_enable <= 1'b0;
                    if(!spi_enable && !spi_busy)
                    begin

                        if((spi_rom_counter==4'd10) && (spi_byte_counter==2'd0))
                            spi_state <= SPI_COMPLETE;
                        else
                            spi_state <= SPI_SEND;
                    end
                end

                SPI_COMPLETE:
                begin
                    spi_state <= SPI_COMPLETE;
                end

                default: spi_state <= SPI_IDLE;
            endcase

        end
    end

    /* ram port b mux */
    /*wire B_enable=spi_complete?buffer_enable:spi_enable;
    wire B_address=spi_complete?buffer_address:{3'd0, spi_rom_counter, spi_byte_counter};
    wire [9:0] B_data_in=spi_complete?buffer_data_in:spi_data;*/

    /* sd double buffered ram */
    reg buffer_enable=1'b0;
    reg buffer=1'b0;
    reg [8:0] buffer_address={9{1'b1}};
    reg [7:0] buffer_data_in=8'd0;
    ram_double_buffer #(.data_width(8), .address_width(9)) ram
                       (.buffer(buffer),
                        .A_clk(sd_ram_clk), .A_enable(sd_ram_enable),
                        .A_write_enable(sd_ram_write_enable),
                        .A_address(sd_ram_address), .A_data_in(sd_ram_data_in),
                        .A_data_out(sd_ram_data_out),
                        //.B_clk(clk), .B_enable(B_enable), .B_write_enable(1'b1),
                        //.B_address(B_address), .B_data_in(B_data_in),
                        .B_clk(clk), .B_enable(buffer_enable), .B_write_enable(1'b1),
                        .B_address(buffer_address), .B_data_in(buffer_data_in),
                        .B_data_out());

    /* buffered ram write logic */
    localparam BUFFER_IDLE=0, BUFFER_FETCH=1, BUFFER_WRITE=2;
    reg [1:0] buffer_state=BUFFER_IDLE;
    reg buffer_empty=1'b1;
    reg [5:0] word_counter='d0;//buffer_address[8:3];
    reg [2:0] byte_counter='d0;//buffer_address[2:0];
    reg [63:0] rd_data;
    reg [31:0] baseband_counter='d1;
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            buffer_state <= BUFFER_IDLE;

            buffer <= 1'b0;
            buffer_empty <= 1'b1;
            buffer_enable <= 1'b0;
            buffer_address <= {9{1'b1}};

            word_counter <= 6'd0;
            byte_counter <= 3'd0;

            write_next <= 1'b0;
            baseband_counter <= 'd1;
        end
        else
        begin
            case(buffer_state)
            BUFFER_IDLE:
            begin
                buffer_enable <= 1'b0;
                write_next <= 1'b0;

                if(buffer_empty)
                begin
                    /* reset fetch counter */
                    word_counter <= 6'd0;
                    buffer_address <= {9{1'b1}};

                    /* move to next state */
                    buffer_state <= BUFFER_FETCH;
                end
                else if(write_next_idle)
                begin
                    /* swap buffers */
                    buffer <= ~buffer;

                    /* mark the new buffer as empty */
                    buffer_empty <= 1'b1;

                    /* start a write */
                    write_next <= 1'b1;

                    baseband_counter <= baseband_counter + 1'b1;
                end
            end

            BUFFER_FETCH:
            begin
                buffer_enable <= 1'b0;
                if(!lpddr_rd_empty)
                begin
                    /* the lpddr will already have the data on its output, if rd_empty is low */
                    rd_data <= lpddr_rd_data;
                    
                    /* enable read - this essentially clears the first word from the fifo */
                    lpddr_rd_en <= 1'b1;

                    /* reset counter for buffer write / output shifting */
                    byte_counter <= 3'd0;

                    /* move to next state */
                    buffer_state <= BUFFER_WRITE;
                end
            end

            BUFFER_WRITE:
            begin
                /* ensure read isn't sent again */
                lpddr_rd_en <= 1'b0;

                /* write byte */
                case(byte_counter)
                    0: buffer_data_in <= rd_data[63:56];
                    1: buffer_data_in <= rd_data[55:48];
                    2: buffer_data_in <= rd_data[47:40];
                    3: buffer_data_in <= rd_data[39:32];
                    4: buffer_data_in <= rd_data[31:24];
                    5: buffer_data_in <= rd_data[23:16];
                    6: buffer_data_in <= rd_data[15:8];
                    7: buffer_data_in <= rd_data[7:0];
                endcase
                buffer_address <= buffer_address + 1'b1;
                buffer_enable <= 1'b1;

                /* increment byte counter */
                byte_counter <= byte_counter + 1'b1;

                /* check for end condition */
                if(byte_counter==3'd7)
                begin
                    word_counter <= word_counter + 1'b1;
                    if(word_counter==6'd63)
                    begin
                        buffer_empty <= 1'b0;
                        buffer_state <= BUFFER_IDLE;
                    end
                    else
                        buffer_state <= BUFFER_FETCH;
                end
            end
            endcase
            /* process
             * - if buffer is empty and lpddr has data
             * - fetch 64 bits from lpddr
             *     - write 64 bits into 8 bit buffer
             * - if buffer is not full fetch next 64 bits
             */
        end
    end

    /* ram read logic */
    wire fifo_empty=(lpddr_rd_address==lpddr_wr_address);
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            lpddr_rd_cmd_en <= 1'b0;
            lpddr_rd_address <= 21'd0;
        end
        else
        begin
            if((!lpddr_rd_cmd_en) && (!fifo_empty) && lpddr_rd_empty && lpddr_rd_cmd_empty)
            begin
                /* there is un-read data stored in the lpddr ram and both the lpddr's data and command
                 * fifos are empty - we check for the command fifo to be empty so we don't issue read
                 * commands directly after each other */

                /* send read command */
                lpddr_rd_cmd_en <= 1'b1;
            end
            else
                lpddr_rd_cmd_en <= 1'b0;

            if(lpddr_rd_cmd_en)
                /* increment address */
                lpddr_rd_address <= lpddr_rd_address + 1'b1;
        end
    end

    /* input control */
    wire button_fb, output_pulse;
    wire [1:0] input_enable;
    wire [2:0] outputs;
    input_control input_con(.clk(clk), .reset(reset), .button(switch), .enable({1'b1,input_enable}),
                            .button_fb(button_fb), .outputs(outputs), .output_pulse(output_pulse));
    assign reset_in=(outputs[2]&output_pulse);

    /* state control */
    wire [1:0] blink_rate;
    wire record_enable, record_finish, complete;
    state_control state_con(.clk(clk), .reset(reset), .blink_rate(blink_rate),
                            .inputs(outputs[1:0]), .input_pulse(output_pulse),
                                .input_enable(input_enable),
                            .sd_idle(sd_idle), .sd_card_detect(sd_card_detect),
                            .spi_complete(spi_complete), .spi_init(spi_init),
                            .record_enable(record_enable), .record_finish(record_finish),
                            .complete(complete));

    /* record control */
    reg command_issued=1'b0;
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            baseband_enable <= 1'b0;
            data_command_enable <= 1'b0;
            command_issued <= 1'b0;
        end
        else
        begin
            /* send write command */
            if(record_enable && !command_issued)
            begin
                if(sd_idle)
                    data_command_enable <= 1'b1;
                else if(data_command_enable && !sd_idle)
                begin
                    data_command_enable <= 1'b0;
                    command_issued <= 1'b1;

                    /* enable baseband */
                    baseband_enable <= 1'b1;
                end
            end
            /* stop when requested */
            else if(record_finish && sd_idle)
                baseband_enable <= 1'b0;
            /* stop when we error or have filled the card */
            else if(complete)
                baseband_enable <= 1'b0;
        end
    end
    assign last_block=((baseband_counter==card_block_size)||record_finish);
    wire fifo_overflow=((lpddr_rd_address[19:0]==lpddr_wr_address[19:0]) &&
                        (lpddr_rd_address[20]!=lpddr_wr_address[20]) &&
                        inc_wr_address);
    assign complete=((sd_error!='d0)||(command_issued && sd_idle)||(wr_error)||(rd_error)||
                     fifo_overflow);

    /* led activity blinker */
    wire led_blink;
    blinker blink(.clk(clk), .reset(reset), .rate(blink_rate), .blink(led_blink));

    /* led assignment */
    assign LED1=(outputs[0]|led_blink);
    assign LED2=button_fb;
    assign LED3=outputs[1];
    assign LED4=outputs[2];

endmodule
