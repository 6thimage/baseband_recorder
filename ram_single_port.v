`timescale 1ns / 1ps

module ram_single_port
    #(
    parameter data_width=8,
    parameter address_width=2
    )
    (
     input                         clk,
     input                         enable,
     input                         write_enable,
     input     [address_width-1:0] address,
     input     [data_width-1:0]    data_in,
    output reg [data_width-1:0]    data_out
    );

    /* memory */
    reg [data_width-1:0] memory[(2**address_width)-1:0];
 
    /* default the memory to zero */
    integer i;
    initial
    begin
        for(i=0; i<(2**address_width); i=i+1)
            memory[i]='h0;
    end

    /* port */
    always @(posedge clk)
    begin
        if(enable)
        begin
            /* write */
            if(write_enable)
                memory[address] <= data_in;

            /* read */
            data_out <= memory[address];
        end
    end

endmodule
