#! /bin/sh

cp -p ../../sd_controller/clk_strobe.v .
cp -p ../../sd_controller/cmd_sender.v .
cp -p ../../sd_controller/crc16_2.v .
cp -p ../../sd_controller/crc7.v .
cp -p ../../sd_controller/mod_m_counter.v .
cp -p ../../sd_controller/resp_recv.v .
cp -p ../../sd_controller/sclk_controller.v .
cp -p ../../sd_controller/sd_command.v .
cp -p ../../sd_controller/sd_controller.v .
cp -p ../../sd_controller/sd_data.v .
