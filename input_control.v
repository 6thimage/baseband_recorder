`timescale 1ns / 1ps

module input_control(
     input clk,
     input reset,
     input button,
     input [2:0] enable,
    output button_fb,
    output reg [2:0] outputs,
    output reg output_pulse
    );

    initial
    begin
        outputs=3'd0;
        output_pulse=1'b0;
    end

    /* synchronise button input to clock */
    wire button_sync;
    synchroniser #(.width(1), .depth(2)) sync(.clk(clk), .signal(button), .sync_signal(button_sync));

    /* debounce button input */
    /* enable signal at 100 Hz (10 ms) */
    wire debounce_enable;
    mod_m_counter #(.m(500_000), .reset_pulse(0))
                  debounce_clk_reduc(.clk(clk), .reset(reset), .enable(1'b1), .hit(debounce_enable));
    /* debounce register */
    localparam debounce_len=5;
    reg [debounce_len-1:0] debounce='d0;
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
            debounce <= 'd0;
        else if(debounce_enable)
            debounce <= {debounce[debounce_len-2:0], button_sync};
    end
    /* keep track of when register is all the same value */
    wire debounce_high=(debounce=={debounce_len{1'b1}});
    assign button_fb=debounce_high;
    reg debounce_high_last=1'b0;
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
            debounce_high_last <= 1'b0;
        else
            debounce_high_last <= debounce_high;
    end
    wire button_down=(debounce_high && !debounce_high_last);
    wire button_up=(!debounce_high && debounce_high_last);

    /* input timing */
    reg [8:0] timer='d0;
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            timer <= 'd0;
            outputs <= 3'd0;
            output_pulse <= 1'b0;
        end
        else
        begin
            if(button_down)
            begin
                timer <= 'd1;
                outputs <= 3'd0;
            end
            else if(button_up)
            begin
                timer <= 'd0;
                output_pulse <= 1'b1;
            end
            else if(debounce_enable && timer!='d0)
            begin
                timer <= timer + 1'b1;

                if(timer=='d50 && enable[0])
                    outputs <= 3'b001;
                if(timer=='d150 && enable[1])
                    outputs <= 3'b010;
                if(timer=='d500 && enable[2])
                    outputs <= 3'b100;
            end

            if(output_pulse)
                output_pulse <= 1'b0;
        end
    end

endmodule
