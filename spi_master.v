`timescale 1ns / 1ps

module spi_master(
    /* clock */
     input           clk,
    /* external connections */
    output reg       ss,        /* slave select */
    output reg       sclk,      /* serial clock */
    output reg       mosi,      /* master out, slave in */
     input           miso,      /* master in, slave out */
    /* internal connections */
     input           reset,     /* reset - active low */
     input           enable,    /* enable / start - starts a spi transaction - active high */
     input     [7:0] clock_div, /* clock divider  - spi clock has a maximum of clk/2, this is
                                                    to reduce it futher */
     input           cpol,      /* sclk polarity  - 0 for start low, 1 for start high */
     input           cpha,      /* sclk phase     - if 0, the first bit is transmitted with the
                                                    falling edge of sclk, if 1 the first bit is
                                                    delayed to the first sclk */
     input     [7:0] tx_data,   /* data to transmit */
    output reg [7:0] rx_data,   /* received data */
     input           data_cont, /* data continue  - if 1, slave select will remain low after
                                                    the byte transaction to allow for further
                                                    data, otherwise the slave select line is
                                                    returned high */
    output           busy       /* busy flag      - is 1 during a transaction, 0 otherwise */
    );

    initial
    begin
        ss=1'b1;
        sclk=cpol;
        mosi=1'b1;
    end

    /* internal data buffers */
    reg [7:0] tx_data_int;
    reg [7:0] rx_data_int=8'hff;

    /* internal config buffers */
    reg [7:0] clock_div_int;
    reg data_cont_int;
    reg cpol_int, cpha_int;
    wire cpol_eq_cpha;
    assign cpol_eq_cpha=(cpol_int==cpha_int);

    /* serial clock - for dividing the main clock down */
    reg [7:0] clock_counter;
    wire clock_counter_hit;
    assign clock_counter_hit=(clock_counter==clock_div_int);

    /* bit counter */
    reg [3:0] bit_counter; /* number of bits sent */
    
    /* clock edges
                    sclk state      edge
                    read    write   read    write
       cpha=0
           cpol=0   0       1       rising  falling
           cpol=1   1       0       falling rising
       cpha=1
           cpol=0   1       0       falling rising
           cpol=1   0       1       rising  falling
       
       as far as clock edges are concerned, there are two situations
       that determine which edge is used for reading and writing
           cpol==cpha
           cpol!=cpha
     */
    wire sclk_receive_edge, sclk_transmit_edge;
    assign sclk_receive_edge=(cpol_eq_cpha)?(sclk==1'b0):(sclk==1'b1);
    assign sclk_transmit_edge=(cpol_eq_cpha)?(sclk==1'b1):(sclk==1'b0);

    localparam STATE_IDLE=0, STATE_DATA=1, STATE_WAIT=2;
    reg [1:0] state=STATE_IDLE;

    assign busy=(state!=STATE_IDLE);

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            /* reset to defaults */
            sclk <= cpol;
            mosi <= 1'b1;
            rx_data_int <= 8'hff;
            state <= STATE_IDLE;
        end
        else
        begin
            case(state)
                STATE_IDLE:
                begin
                    /* detect start */
                    if(enable)
                    begin
                        /* reset counters */
                        clock_counter <= 8'h0;
                        /* set bit counter to 0 */
                        bit_counter <= 4'h0;

                        /* update config */
                        clock_div_int <= clock_div;
                        data_cont_int <= data_cont;
                        cpol_int <= cpol;
                        cpha_int <= cpha;

                        /* start transaction */
                        state <= STATE_DATA;
                        ss <= 1'b0;
                        sclk <= cpol;

                        /* send first bit if cpha=0 */
                        mosi <= tx_data[7];
                        if(!cpha)
                        begin
                            /* shift tx data accordingly */
                            tx_data_int <= {tx_data[6:0],1'b0};
                        end
                        else
                            tx_data_int <= tx_data;
                    end
                end

                STATE_DATA:
                begin
                    if(clock_counter_hit)
                    begin
                        /* reset counter */
                        clock_counter <= 8'h0;

                        /* update clock */
                        if(bit_counter!=4'h8)
                            sclk <= ~sclk; /* toggle clock */

                        /* update bit counter */
                        if(sclk!=cpol_int)
                            bit_counter <= bit_counter+1'b1;

                        if(bit_counter!=4'h8)
                        begin
                            if(sclk_transmit_edge)
                                {mosi, tx_data_int} <= {tx_data_int, tx_data_int[7]};
                            else if(sclk_receive_edge)
                                rx_data_int <= {rx_data_int[6:0], miso};
                        end
                        else
                        begin
                            /* disable transmission */
                            state <= STATE_WAIT;

                            clock_counter <= 8'h0;
                            bit_counter <= 4'h0;

                            /* output received data */
                            rx_data <= rx_data_int;
                        end
                    end
                    else
                        /* increment counter */
                        clock_counter <= clock_counter + 1'b1;
                end

                STATE_WAIT:
                begin
                    if(data_cont_int)
                        ss <= 1'b0;
                    else
                        ss <= 1'b1;

                    if(clock_counter_hit)
                    begin
                        clock_counter <= 8'h0;

                        if(bit_counter==8'd1)
                            state <= STATE_IDLE;

                        bit_counter <= bit_counter + 1'b1;
                    end
                    else
                        clock_counter <= clock_counter + 1'b1;
                end

                default: state <= STATE_IDLE;
            endcase
        end
    end

endmodule
