`timescale 1ns / 1ps
module state_control(
     input clk,
     input reset,
    /* blinker */
    output reg [1:0] blink_rate,
    /* input */
     input [1:0] inputs,
     input input_pulse,
    output reg [1:0] input_enable,
    /* sd card */
     input sd_idle,
    output reg sd_card_detect,
    /* spi master */
     input spi_complete,
    output reg spi_init,
    /* record logic */
    output reg record_enable,
    output reg record_finish,
     input complete
    );

    initial
    begin
        blink_rate=2'd1;
        input_enable=2'b10;
        sd_card_detect=1'b0;
        spi_init=1'b0;
        record_enable=1'b0;
        record_finish=1'b0;
    end

    localparam STATE_IDLE=0, STATE_INIT=1, STATE_LOG=2, STATE_END=3;
    reg [1:0] state=STATE_IDLE;

    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            blink_rate <= 2'd1;
            input_enable <= 2'b10;
            sd_card_detect <= 1'b0;
            spi_init <= 1'b0;
            record_enable <= 1'b0;
            record_finish <= 1'b0;

            state <= STATE_IDLE;
        end
        else
        begin
            case(state)
            STATE_IDLE:
            begin
                blink_rate <= 2'd1;
                input_enable <= 2'b10;

                if(input_pulse && inputs[1])
                    /* move to init state */
                    state <= STATE_INIT;
            end

            STATE_INIT:
            begin
                /* with card_detect=0, sd_idle will be low */
                sd_card_detect <= 1'b1;

                spi_init <= 1'b1;

                if(complete)
                    state <= STATE_END;
                else if(sd_idle && spi_complete)
                begin
                    blink_rate <= 2'd1;
                    input_enable <= 2'b01;

                    if(input_pulse && inputs[0])
                    begin
                        state <= STATE_LOG;
                        input_enable <= 2'b10;
                        blink_rate <= 2'd3;
                    end
                end
                else
                begin
                    blink_rate <= 2'd0;
                    input_enable <= 2'b00;
                end
            end

            STATE_LOG:
            begin
                record_enable <= 1'b1;

                if(input_pulse && inputs[1])
                begin
                    input_enable <= 2'b00;
                    blink_rate <= 2'd0;
                    record_finish <= 1'b1;
                end

                if(complete)
                    state <= STATE_END;
            end

            STATE_END:
            begin
                blink_rate <= 2'd2;
                input_enable <= 2'b00;
                record_enable <= 1'b0;
            end
            endcase
        end
    end

endmodule
