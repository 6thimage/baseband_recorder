`timescale 1ns / 1ps

module baseband(
     input clk,
     input reset,
     input enable,
     input data_clk,
     input [1:0] I,
     input [1:0] Q,
    output wr_en,
    output reg [63:0] wr_data
    );

    initial wr_data=64'd0;

    /* synchronise inputs */
    localparam sync_depth=2;
    wire data_clk_sync;
    wire [1:0] I_sync, Q_sync;
    synchroniser #(.width(1), .depth(sync_depth), .init_high(0))
                 sync_data_clk(.clk(clk), .signal(data_clk), .sync_signal(data_clk_sync));
    synchroniser #(.width(2), .depth(sync_depth), .init_high(0))
                 sync_I(.clk(clk), .signal(I), .sync_signal(I_sync));
    synchroniser #(.width(2), .depth(sync_depth), .init_high(0))
                 sync_Q(.clk(clk), .signal(Q), .sync_signal(Q_sync));

    /* look for falling edge of data_clk */
    reg data_clk_sync_prev=1'b0;
    always @(posedge clk or negedge reset)
    begin
        if(!reset) data_clk_sync_prev <= 1'b0;
        else if(enable) data_clk_sync_prev <= data_clk_sync;
    end
    wire data_clk_edge=(data_clk_sync_prev && !data_clk_sync)?1'b1:1'b0; /* falling edge */

    /* write output */
    reg [3:0] wr_counter=4'd0;
    reg last_wr_zero=1'b1;
    /* this pulses wr_en for one clock when wr_counter equals zero */
    assign wr_en=((wr_counter==4'd0) && (!last_wr_zero));
    always @(posedge clk or negedge reset)
    begin
        if(!reset)
        begin
            wr_data <= 64'd0;
            wr_counter <= 4'd0;
            last_wr_zero <= 1'b1;
        end
        else
        begin
            if(enable && data_clk_edge)
            begin
                /* shift in data */
                wr_data <= {wr_data[59:0], I_sync, Q_sync}; /* {wr_data[59:0], I1, I0, Q1, Q0} */

                /* increment counter */
                wr_counter <= wr_counter + 1'b1;
            end

            /* update last_wr_zero */
            last_wr_zero <= (wr_counter==4'd0);
        end
    end

endmodule
