`timescale 1ns / 1ps

module crc7(
     input           clk,
     input           reset,
     input           enable,
     input     [7:0] data,
    output reg [6:0] crc
    );

    initial crc=7'h0;

    always @(posedge clk or negedge reset)
    begin
        /* on reset, set the crc high */
        if(!reset)
            crc <= 7'h0;
        else if(enable)
        begin
            /* process input data byte, if enabled */
            crc[0] <= crc[3] ^ crc[6] ^ data[0] ^ data[4] ^ data[7];
            crc[1] <= crc[0] ^ crc[4] ^ data[1] ^ data[5];
            crc[2] <= crc[1] ^ crc[5] ^ data[2] ^ data[6];
            crc[3] <= crc[2] ^ crc[3] ^ data[0] ^ data[3] ^ data[4];
            crc[4] <= crc[0] ^ crc[3] ^ crc[4]  ^ data[1] ^ data[4] ^ data[5];
            crc[5] <= crc[1] ^ crc[4] ^ crc[5]  ^ data[2] ^ data[5] ^ data[6];
            crc[6] <= crc[2] ^ crc[5] ^ crc[6]  ^ data[3] ^ data[6] ^ data[7];
        end
    end

endmodule
