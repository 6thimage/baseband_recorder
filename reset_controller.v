`timescale 1ns / 1ps

module reset_controller(
     input     clk,
     input     reset_in,
    output reg reset_out
    );

    initial reset_out=1'b0;

    reg [10:0] reset_counter=11'h0;

    always @(posedge clk or negedge reset_in)
    begin
        if(!reset_in)
        begin
            reset_out <= 1'b0;
            reset_counter <= 11'h0;
        end
        else if(!reset_out)
        begin
            if(reset_counter==11'd2000)
                reset_out <= 1'b1;
            else
                reset_counter <= reset_counter +1'b1;
        end
    end

endmodule
